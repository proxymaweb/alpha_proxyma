$(document).ready(function() {

    // ENLACES ANCLA PARA SCROLL VERTICAL

    $("#link_characters_m").click(function () {
        var position = $("#home-characters_m").offset().top;
        $("html, body").animate({
            scrollTop: position - 68
        }, 800);
        return false;
    });

    $("#link_zestrias_m").click(function () {
        var position = $("#home-zestrias_m").offset().top;
        $("html, body").animate({
            scrollTop: position - 68
        }, 800);
        return false;
    });

    $("#link_sincro_m").click(function () {
        var position = $("#home-sincro_m").offset().top;
        $("html, body").animate({
            scrollTop: position - 68
        }, 800);
        return false;
    });

    $("#link_world_m").click(function () {
        var position = $("#home-world_m").offset().top;
        $("html, body").animate({
            scrollTop: position - 68
        }, 800);
        return false;
    });

    // MENU MOBILE

    $("#menuMobile").click(function(){
        $("#header-menu").toggle("slide");
    });

    // SWIPER
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 'auto',
        centeredSlides: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30
    });

    // UNLOCK

    $("#cerrojo_key").on('keydown', function(ev){
        if(ev.which === 13){
            if($("#cerrojo_key").val() == 'zestrias'){
                abrirPuerta();
            }else{
                denied();
            }
        }
    })

});

function denied(){
    $(".nope").show("slow");
    setTimeout(function(){
        $(".nope").hide("slow");
    }, 2000);
}

function abrirPuerta(){
    $("#puerta-cerrojo").css('transform', 'rotate(90deg)');
    setTimeout(function(){
        $("#puerta-abajo").css('animation', 'abajo 1000ms ease-in');
        $("#puerta-arriba").css('animation', 'arriba 1000ms ease-in');
        setTimeout(function(){
            $("#puerta-arriba").css('top', 'calc(-50vh + -100px)');
            $("#puerta-abajo").css('bottom', '-50vh');
        }, 1000);
    },600);
}





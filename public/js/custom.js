$(document).ready(function() {

    // ENLACES ANCLA PARA SCROLL VERTICAL

    $("#link_characters").click(function () {
        var position = $("#home-characters").offset().top;
        $("html, body").animate({
            scrollTop: position - 92
        }, 800);
        return false;
    });

    $("#link_zestrias").click(function () {
        var position = $("#home-zestrias").offset().top;
        $("html, body").animate({
            scrollTop: position - 92
        }, 800);
        return false;
    });

    $("#link_sincro").click(function () {
        var position = $("#home-sincro").offset().top;
        $("html, body").animate({
            scrollTop: position - 92
        }, 800);
        return false;
    });

    $("#link_world").click(function () {
        var position = $("#home-world").offset().top;
        $("html, body").animate({
            scrollTop: position - 92
        }, 800);
        return false;
    });

    $("#cerrojo_key").on('keydown', function(ev){
        if(ev.which === 13){
            if($("#cerrojo_key").val() == 'zestrias'){
                abrirPuerta();
            }else{
                denied();
            }
        }
    })

});

function denied(){
    $(".nope").show("slow");
    setTimeout(function(){
        $(".nope").hide("slow");
    }, 2000);
}

function abrirPuerta(){
    $("#puerta-cerrojo").css('transform', 'rotate(90deg)');
    setTimeout(function(){
        $("#puerta-abajo").css('animation', 'abajo 1000ms ease-in');
        $("#puerta-arriba").css('animation', 'arriba 1000ms ease-in');
        setTimeout(function(){
            $("#puerta-arriba").css('top', 'calc(-50vh + -100px)');
            $("#puerta-abajo").css('bottom', '-50vh');
        }, 1000);
    },600);
}


function changeCharacter(character,description) {
$("#description-characters").html(description);
$("#characterShow").attr("src", "images/pj/"+character+".png");

/*$("#characterShow").animate({width:'toggle'},350);
setTimeout(
    function(){

        $("#description-characters").html(description);


        }


        $("#characterShow").attr("src", "images/pj/"+character+".png");
        $("#characterShow").animate({width:'toggle'},350)
    }, 1000);*/
}


function changeZestria(zestria,description) {

    $("#zestriaShow").animate({width:'toggle'},350);
    setTimeout(
        function(){
              $("#description-zestrias").html(description);
            $("#zestriaShow").attr("src", "images/zestrias/"+zestria+".png");
            $("#zestriaShow").animate({width:'toggle'},350)
        }, 1000);
      }



function changeSincro(sincro,description) {
  alert(description);
    $("#sincroShow").animate({width:'toggle'},350);
    setTimeout(
        function(){
            $("#description-synchro").html(description);
            $("#sincroShow").attr("src", "images/sincro/"+sincro+"_synchro.png");
            $("#sincroShow").animate({width:'toggle'},350)
        }, 1000);
}


function changeWorld(world) {

    var desc_alkoi = "<b>Alkoi</b><br><span>This sprawling metropolis, hosts people from all over Proxyma. The Schwartz’s Headquarters are located here, as well as one of the mysterious towers.</span>";

    var desc_wagens = "<b>Wagens</b></br><span>A peaceful nation, full of lush forests and green valleys, is where Kyron’s hometown is found. An old civilisation is said to have lived here centuries ago, leaving behind them, an ancient culture and most important, architectural heritage.</span>";

    var desc_waro = "<b>Waro</b><br><span>One of the most unforgiving areas in all proxyma, it is full of snowy, rocky and very dangerous mountains. At a first glance, it seems like so, but in the underground, inside the mountains, there exists a complex cave system where workers that are exploited live and work, controlled by Schwartz.</span>";

    var desc_garodam ="<b>Garodam/Arunda</b><br><span>Deep and dense jungles cover all the territory, serving as a place where tribesmen gather food, hunt and live, in their particular rooftop houses and cabins.</span>";

    var desc_koro_vallis ="<b>Koro Vallis</b><br><span>The one that stands between the advanced Alkoi and the almost prehistoric Arunda. Its most characteristic feature is the great wall, constructed by the elitists at Alkoi, to stop the flux of immigrants coming from the South, bringing, in their eyes, crime and disease.</span>";

    var desc_insula ="<b>Insula</b><br><span>The most centered region in Proxyma, it acts as the middleman between all regions and continents. Its inhabitants live in the sea, afraid of all the  misteries that the islands hide, from all those old legends.</span>";

    var desc_hellus ="<b>Hellus</b><br><span>Toxic sulfur gases and an active volcano define the region. The sun rays do not reach this land due to the poisonous fog. Scary zestrias and almost no vegetation can be found there. Hellus is considered the most dangerous and deathly zone in all of Proxyma.</span>";

    var desc_pumthemis ="<b>Pumthemis</b><br><span>The most septentrional region in Proxyma and the coldest one. It is entirely covered by ice and snow. Huge mountains spread through the whole region, slowly turning into ice plains with frozen lakes that reach the sea.</span>";

    switch (world) {
      case  'alkoi':
            $("#home-world").attr( "class", world );
            $("#description-world").html(desc_alkoi);
            break;
      case  'wagens':
            $("#home-world").attr( "class", world );
            $("#description-world").html(desc_wagens);
            break;
      case 'waro':
            $("#home-world").attr( "class", world );
            $("#description-world").html(desc_waro);
            break;
      case 'garodam':
            $("#home-world").attr( "class", world );
            $("#description-world").html(desc_garodam);
            break;
      case  'koro_vallis':
            $("#home-world").attr( "class", world );
            $("#description-world").html(desc_koro_vallis);
            break;
      case 'insula':
            $("#home-world").attr( "class", world );
            $("#description-world").html(desc_insula);
            break;
      case 'hellus':
            $("#home-world").attr( "class", world );
            $("#description-world").html(desc_hellus);
            break;
      case 'pumthemis':
            $("#home-world").attr( "class", world );
            $("#description-world").html(desc_pumthemis);
            break;

      default:
      $("#home-world").attr( "class", world );
      $("#description-world").html(desc_alkoi);
      break;
    }

}

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the home page.
    |
    */

    'characters' => 'PERSONAJES',
    'zestrias' => 'ZESTRIAS',
    'synchro' => 'SINCRO',
    'world' => 'MUNDO',
    'project' => 'EL PROYECTO',
    'blog' => 'BLOG',
    'about' => 'ABOUT',
    'download' => 'Descargar Artbook',
    'moreinfo' => 'Más info >>',

];
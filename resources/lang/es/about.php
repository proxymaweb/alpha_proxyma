<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the home page.
    |
    */

    'aboutproject' => 'SOBRE EL PROJECTO',
    'aboutteam' => 'SOBRE EL EQUIPO',
    'contact' => 'CONTACTA CON NOSOTROS',
    'name' => 'NOMBRE',
    'email' => 'EMAIL',
    'message' => 'MENSAJE',
    'send' => 'ENVIAR',

];
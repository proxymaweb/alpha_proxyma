<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the home page.
    |
    */

    'aboutproject' => 'ABOUT THE PROJECT',
    'aboutteam' => 'ABOUT THE TEAM',
    'contact' => 'CONTACT US',
    'name' => 'NAME',
    'email' => 'EMAIL',
    'message' => 'MESSAGE',
    'send' => 'SEND',

];
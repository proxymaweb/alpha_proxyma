<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the home page.
    |
    */

    'characters' => 'CHARACTERS',
    'zestrias' => 'ZESTRIAS',
    'synchro' => 'SYNCHRO',
    'world' => 'WORLD',
    'project' => 'THE PROJECT',
    'blog' => 'BLOG',
    'about' => 'ABOUT',
    'download' => 'Download Artbook',
    'moreinfo' => 'More info >>',

];
<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>The Eternal Shepherd - Towers of Proxyma</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{url('images/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" href="{{url('images/favicon/favicon-32x32.png')}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{url('images/favicon/favicon-16x16.png')}}" sizes="16x16">
    <link rel="manifest" href="{{url('images/favicon/manifest.json')}}">
    <link rel="mask-icon" href="{{url('images/favicon/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/foundation/foundation.css')}}">
    <link rel="stylesheet" href="{{url('css/app.css')}}">
    <link rel="stylesheet" href="{{url('css/custom/stylesheets/custom.css')}}">
    <link rel="stylesheet" href="{{url('css/custom/stylesheets/custom_mobile.css')}}">

    <script type="text/javascript">
      <!--
      if (screen.width <= 639) {
        {{ redirect('./home_mobile.html')}}
      }
      //-->
    </script>

  </head>
  <body>

    <!-- Header -->

    <header>
      <div class="row">
        <div class="small-4 medium-4 large-3 columns text-left">
          <a href="/">
            <img src="images/logo.png" class="header-logo"/>
          </a>
        </div>
        <div class="small-8 medium-8 large-9 columns">
          <div id="header-menu">
            <div style="float: right">
              <img src="images/UnitedKingsom.png" class="header-language"/>
              <img src="images/Spain.png" class="header-language"/>
            </div>
            <ul>
              <li>
                @lang('home.project')
                <ul class="header-submenu">
                  <a href="{{url('/characters')}}">
                    <li>@lang('home.characters')</li>
                  </a>
                  <a href="{{url('/zestrias')}}">
                    <li>@lang('home.zestrias')</li>
                  </a>
                  <a href="{{url('/synchro')}}">
                    <li>@lang('home.synchro')</li>
                  </a>
                  <a href="{{url('/world')}}">
                    <li>@lang('home.world')</li>
                  </a>
                </ul>
              </li>
              <a href="{{url('/blog')}}">
                <li>@lang('home.blog')</li>
              </a>
              <a href="{{url('/about')}}">
                <li>@lang('home.about')</li>
              </a>
            </ul>
          </div>
        </div>
      </div>
    </header>

    <!-- HOME -->

    <main>
      <div class="topnav-space"></div>
      <div id="home-portal">
        <div class="row">
          <div class="small-12 large-12 column text-center">
            <img src="images/logo.png" class="home-logo">
              <a href="recursos/HmhStudios-Artbook.pdf" download="HmhStudios-TheEternalShepherd" class="download-artbook">
                <span>@lang('home.download')</span>
              </a>
          </div>
        </div>
        <div class="row">
          <div class="small-12 large-12 column text-center">
            <div class="home-portal-left">
              <section id="link_characters" class="home-portal-section section-1">
                @lang('home.characters')
              </section>

              <section id="link_zestrias" class="home-portal-section section-2">
                @lang('home.zestrias')
                <?php
                  /*$prox_zestrias = App\prox_zestrias::all();

                 foreach ($prox_zestrias as $prox_zestria) {
                   echo $prox_zestria->zest_desc;
                 }*/
                 ?>
              </section>
            </div>
            <section class="home-portal-center">
              <iframe width="396" height="236" src="https://www.youtube.com/embed/dCHdscWRCBA" frameborder="0" allowfullscreen></iframe>
            </section>
            <div class="home-portal-right">
              <section id="link_sincro" class="home-portal-section section-3">
                @lang('home.synchro')
              </section>
              <section id="link_world" class="home-portal-section section-4">
                {{ __('home.world') }}
              </section>
            </div>
          </div>
        </div>
      </div>

      <div id="home-characters" class="section-1">
        <div class="row margin-0">
          <div class="large-4 medium-4 columns" style="background: rgba(0,0,0,0.8);height: 512px;">
            <div class="home-section-title">@lang('home.characters')</div>
            <div class="home-section-description" id="description-characters">
              <b>Kyron</b><br>
              <span>Kyron is 16 when the army destroys his village in Wagens and his family. After discovering his true nature as shepherd, he starts his journey with Kintia and Temy, his Zestria and best friend, to find the keys and protect Proxyma.</span>
            </div>
            <div class="home-section-wrapper-icons">
              <?php
                $characters = App\prox_characters::all();

               foreach ($characters as $character) {
              ?>
                <div class="home-section-icon <?=$character->char_name?>"
                     onclick="changeCharacter('<?=$character->char_name?>','<?=$character->char_desc?>')" ></div>
              <?php
               }
               ?>


            </div>
            <div class="home-section-moreinfo">
              <a href="/">
                @lang('home.moreinfo')
              </a>
            </div>
          </div>
          <div class="large-8 columns text-center">
            <div class="home-section-characterZestria">
              <img id="characterShow" src="images/pj/kyron.png" class="characterPersonaje">
            </div>
          </div>
        </div>
      </div>

      <div id="home-zestrias" class="section-2">
        <div class="row margin-0">
          <div class="large-8 columns text-center">
            <div class="home-section-characterZestria">
              <img id="zestriaShow" src="images/zestrias/temmy.png" class="characterZestria">
            </div>
          </div>
          <div class="large-4 medium-4 columns" style="background: rgba(0,0,0,0.8);height: 512px;">
            <div class="home-section-title">@lang('home.zestrias')</div>
            <div class="home-section-description" id="description-zestrias">
              <b>Temmy</b><br>
              <span>Temmy is a blue tadpole zestria with wings. It’s Kyron’s partner and best friend. They met when Temmy was born from an egg that Kyron had found inside some ruins in the woods when he was 5 years old.</span>
            </div>
            <div class="home-section-wrapper-icons">
              <?php
                $zestrias = App\prox_zestrias::all();

               foreach ($zestrias as $zestria) {
              ?>
                <div class="home-section-icon <?=$zestria->zest_name?>"
                     onclick="changeZestria('<?=$zestria->zest_name?>','<?=$zestria->zest_desc?>')" ></div>
              <?php
               }
               ?>
              <!--
              <div class="home-section-icon temmy" onclick="changeZestria('temmy')" ></div>
              <div class="home-section-icon neri" onclick="changeZestria('neri')" ></div>
              <div class="home-section-icon mel" onclick="changeZestria('mel')" ></div>
              <div class="home-section-icon topin" onclick="changeZestria('topin')" ></div>
              <div class="home-section-icon aki" onclick="changeZestria('aki')" ></div>

            -->
              </div>
            <div class="home-section-moreinfo">
              <a href="/">
                @lang('home.moreinfo')
              </a>
            </div>
          </div>
        </div>
      </div>

      <div id="home-sincro" class="section-3">
        <div class="row margin-0">
          <div class="large-4 medium-4 columns" style="background: rgba(0,0,0,0.8);height: 512px;">
            <div class="home-section-title">@lang('home.synchro')</div>
            <div class="home-section-description" id="description-synchro">
              The synchro is a magic ability that appeared after the orb shattered.
              Its fragments, the crystals  that are needed to perform it, are inside the zestrias.
              It allows the human to fuse with the zestria, thus a new form is born with new powers.
              Just a few people are able to perform it, they are known as the synblers.
            </div>
            <div class="home-section-wrapper-icons">
              <?php
                $sincro = App\prox_synchro::all();

               foreach ($sincro as $synchro) {
              ?>
                <div class="home-section-icon <?=$synchro->sync_name?>"
                     onclick="changeSincro('<?=$synchro->sync_name?>','<?=$synchro->sync_desc?>')" ></div>
              <?php
               }
               ?>



              <!--


              <div class="home-section-icon kyron-s" onclick="changeSincro('kyron')" ></div>
              <div class="home-section-icon kinttia-s" onclick="changeSincro('kinttia')" ></div>
              <div class="home-section-icon ylva-s" onclick="changeSincro('ylva')" ></div>
              <div class="home-section-icon zoe-s" onclick="changeSincro('zoe')" ></div>
              <div class="home-section-icon alastor-s" onclick="changeSincro('alastor')" ></div>
                -->
            </div>
            <div class="home-section-moreinfo">
              <a href="/">
                @lang('home.moreinfo')
              </a>
            </div>
          </div>
          <div class="large-8 columns text-center">
            <div class="home-section-characterZestria">
              <img id="sincroShow" src="images/sincro/kyron_synchro.png" class="characterSincro">
            </div>
          </div>
        </div>
      </div>

      <div id="home-world" class="alkoi">
        <div class="row margin-0">
          <div class="large-8 columns text-center"></div>
          <div class="large-4 medium-4 columns" style="background: rgba(0,0,0,0.8);height: 512px;">
            <div class="home-section-title">@lang('home.world')</div>
            <div class="home-section-description" id="description-world">
              <b>Alkoi</b><br>
              <span>This sprawling metropolis, hosts people from all over Proxyma. The Schwartz’s Headquarters are located here, as well as one of the mysterious towers.</span>
            </div>
            <div class="home-section-wrapper-icons">
              <div class="home-section-icon world1-m" onclick="changeWorld('alkoi')" ></div>
              <div class="home-section-icon world2-m" onclick="changeWorld('pumthemis')" ></div>
              <div class="home-section-icon world3-m" onclick="changeWorld('koro_vallis')" ></div>
              <div class="home-section-icon world4-m" onclick="changeWorld('garodam')" ></div>
              <div class="home-section-icon world5-m" onclick="changeWorld('wagens')" ></div>
              <div class="home-section-icon world6-m" onclick="changeWorld('insula')" ></div>
              <div class="home-section-icon world7-m" onclick="changeWorld('hellus')" ></div>
              <div class="home-section-icon world8-m" onclick="changeWorld('waro')" ></div>
            </div>
            <div class="home-section-moreinfo">
              <a href="/">
                @lang('home.moreinfo')
              </a>
            </div>
          </div>
        </div>
      </div>
    </main>

    <!-- FOOTER -->

    <footer>
      <div class="row">
        <div class="large-12 column text-center" style="line-height: 30px;">
          <div class="footer-wrapper">
            <div class="footer-text-wrapper">
              All rights reserved to &copy; <?php echo date("Y"); ?> Hmhstudios |
            </div>
            <div class="footer-social-wrapper">
              <a href="https://www.facebook.com/hmhstudios1" target="_blank">
                <svg class="icon-facebook footer-social">
                  <use xlink:href="images/icons.svg#icon-facebook"></use>
                </svg>
              </a>
              <a href="https://twitter.com/hmh_studios" target="_blank">
                <svg class="icon-twitter footer-social">
                  <use xlink:href="images/icons.svg#icon-twitter"></use>
                </svg>
              </a>
              <a href="https://www.instagram.com/hmhstudios/" target="_blank">
                <svg class="icon-instagram footer-social">
                  <use xlink:href="images/icons.svg#icon-instagram"></use>
                </svg>
              </a>
              <a href="https://www.youtube.com/watch?v=dCHdscWRCBA" target="_blank">
                <svg class="icon-youtube footer-social">
                  <use xlink:href="images/icons.svg#icon-youtube" style="fill: #777777;"></use>
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- OCULTACION -->

    <!--<section id="hidden-door">
      <div id="puerta-arriba">
        <div id="puerta-cerrojo">
          <input id="cerrojo_key" type="password" required>
          <img src="images/denied.gif" class="nope" />
        </div>
      </div>
      <div id="puerta-abajo"></div>
    </section>-->

    <script src="{{url('js/vendor/jquery.js')}}"></script>
    <script src="{{url('js/vendor/what-input.js')}}"></script>
    <script src="{{url('js/vendor/foundation.js')}}"></script>
    <script src="{{url('js/app.js')}}"></script>
    <script src="{{url('js/custom.js')}}"></script>
  </body>
</html>

<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>The Eternal Shepherd - Towers of Proxyma</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{url('images/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" href="{{url('images/favicon/favicon-32x32.png')}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{url('images/favicon/favicon-16x16.png')}}" sizes="16x16">
    <link rel="manifest" href="{{url('images/favicon/manifest.json')}}">
    <link rel="mask-icon" href="{{url('images/favicon/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/foundation/foundation.css')}}">
    <link rel="stylesheet" href="{{url('css/app.css')}}">
    <link rel="stylesheet" href="{{url('css/custom/stylesheets/custom.css')}}">
    <link rel="stylesheet" href="{{url('css/custom/stylesheets/custom_mobile.css')}}">

    <script type="text/javascript">
      <!--
      if (screen.width <= 639) {
        {{ redirect('./home_mobile.html')}}
      }
      //-->
    </script>

  </head>
  <body style="background-image: url('images/home_background.jpg'); background-size: cover; background-attachment: fixed;">

  <!-- Header -->

    <header>
      <div class="row">
        <div class="small-4 medium-4 large-3 columns text-left">
          <a href="/">
            <img src="images/logo.png" class="header-logo"/>
          </a>
        </div>
        <div class="small-8 medium-8 large-9 columns">
          <div id="header-menu">
            <div style="float: right">
              <img src="images/UnitedKingsom.png" class="header-language"/>
              <img src="images/Spain.png" class="header-language"/>
            </div>
            <ul>
              <li>
                @lang('home.project')
                <ul class="header-submenu">
                  <a href="{{url('/characters')}}">
                    <li>@lang('home.characters')</li>
                  </a>
                  <a href="{{url('/zestrias')}}">
                    <li>@lang('home.zestrias')</li>
                  </a>
                  <a href="{{url('/synchro')}}">
                    <li>@lang('home.synchro')</li>
                  </a>
                  <a href="{{url('/world')}}">
                    <li>@lang('home.world')</li>
                  </a>
                </ul>
              </li>
              <a href="{{url('/blog')}}">
                <li>@lang('home.blog')</li>
              </a>
              <a href="{{url('/about')}}">
                <li>@lang('home.about')</li>
              </a>
            </ul>
          </div>
        </div>
      </div>
    </header>

  <!-- HOME -->

  <main>
      <div class="topnav-space"></div>
      <div id="about-project">
          <div style="background: rgba(0,0,0,0.4);">
            <div class="row">
                <div class="small-12 column text-center about-title">
                  <img src="/images/favicon/main-icon.png" class="icon-title"> 
                  @lang('about.aboutproject')
                  <img src="/images/favicon/main-icon.png" class="icon-title"> 
                </div>
            </div>
          </div>
          <div style="background: rgba(225,225,225,0.6);">
            <div class="row">
                <div class="small-8 small-centered column text-center" style="padding: 60px 0 100px;">
                  <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                  <br>
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                  </p>
                  <img src="/images/photo_group.png" style="width: 90%; margin-top: 30px; border: 4px solid rgba(0,0,0,0.4);"> 
                </div>
            </div>
          </div>
      </div>
      <div id="about-team">
          <div style="background: rgba(0,0,0,0.4);">
            <div class="row">
                <div class="small-12 column text-center about-title">
                  <img src="/images/favicon/main-icon.png" class="icon-title"> 
                  @lang('about.aboutteam')
                  <img src="/images/favicon/main-icon.png" class="icon-title"> 
                </div>
            </div>
          </div>
          <div style="background: rgba(225,225,225,0.6);">
            <div class="row">
                <div class="small-8 small-centered column text-center" style="padding: 60px 0 100px;">
                   <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                  </p>
                  <div class="row members-wrapper">
                    <div class="large-3 medium-4 small-6 columns text-center member-wrap">
                      <div class="member-portrait"></div>
                      <span>J. Oriol Borrull</span>
                    </div>
                    <div class="large-3 medium-4 small-6 columns text-center member-wrap">
                      <div class="member-portrait"></div>
                      <span>Eduardo Reyes</span>
                    </div>
                    <div class="large-3 medium-4 small-6 columns text-center member-wrap">
                      <div class="member-portrait"></div>
                      <span>Elia Garcia</span>
                    </div>
                    <div class="large-3 medium-4 small-6 columns text-center member-wrap">
                      <div class="member-portrait"></div>
                      <span>Cintia Sabater</span>
                    </div>
                    <div class="large-3 medium-4 small-6 columns text-center member-wrap">
                      <div class="member-portrait"></div>
                      <span>Arnau Barrabeig</span>
                    </div>
                    <div class="large-3 medium-4 small-6 columns text-center member-wrap">
                      <div class="member-portrait"></div>
                      <span>Alex Caballer</span>
                    </div>
                    <div class="large-3 medium-4 small-6 columns text-center member-wrap">
                      <div class="member-portrait"></div>
                      <span>Xiao Peng</span>
                    </div>
                  </div>
                </div>
            </div>
          </div>
      </div>
      <div id="contact">
          <div style="background: rgba(0,0,0,0.4);">
            <div class="row">
                <div class="small-12 column text-center about-title">
                  <img src="/images/favicon/main-icon.png" class="icon-title"> 
                  @lang('about.contact')
                  <img src="/images/favicon/main-icon.png" class="icon-title"> 
                </div>
            </div>
          </div>
          <div style="background: rgba(225,225,225,0.6);">
            <div class="row" style="padding:50px 0;">
                <div class="small-8 small-centered column text-center">
                  <p></p>
                  <form action="" method="">
                    <div class="row" style="padding: 0 5%;">
                      <div class="small-12 columns title-label">
                      @lang('about.name')
                      </div>
                      <div class="small-12 columns">
                        <input type="text" required>
                      </div>
                    </div>
                    <div class="row" style="padding: 0 5%;">
                      <div class="small-12 columns title-label">
                      @lang('about.email')
                      </div>
                      <div class="small-12 columns">
                        <input type="mail" required>
                      </div>
                    </div>
                    <div class="row" style="padding: 0 5%;">
                      <div class="small-12 columns title-label" >
                      @lang('about.message')
                      </div>
                      <div class="small-12 columns">
                        <textarea rows="5" width="100%"></textarea>
                      </div>
                    </div>
                    <div class="row">
                      <div class="small-6 small-centered columns">
                        <button type="submit">@lang('about.send')</button>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
          </div>
      </div>
    
  </main>

  <!-- FOOTER -->

  <footer>
    <div class="row">
      <div class="large-12 column text-center" style="line-height: 30px;">
        <div class="footer-wrapper">
          <div class="footer-text-wrapper">
            All rights reserved to &copy; <?php echo date("Y"); ?> Hmhstudios |
          </div>
          <div class="footer-social-wrapper">
            <a href="https://www.facebook.com/hmhstudios1" target="_blank">
              <svg class="icon-facebook footer-social">
                <use xlink:href="images/icons.svg#icon-facebook"></use>
              </svg>
            </a>
            <a href="https://twitter.com/hmh_studios" target="_blank">
              <svg class="icon-twitter footer-social">
                <use xlink:href="images/icons.svg#icon-twitter"></use>
              </svg>
            </a>
            <a href="https://www.instagram.com/hmhstudios/" target="_blank">
              <svg class="icon-instagram footer-social">
                <use xlink:href="images/icons.svg#icon-instagram"></use>
              </svg>
            </a>
            <a href="https://www.youtube.com/watch?v=dCHdscWRCBA" target="_blank">
              <svg class="icon-youtube footer-social">
                <use xlink:href="images/icons.svg#icon-youtube" style="fill: #777777;"></use>
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
  </footer>

  </body>
</html>
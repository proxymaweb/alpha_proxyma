<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>The Eternal Shepherd - Towers of Proxyma</title>

    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="images/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="images/favicon/manifest.json">
    <link rel="mask-icon" href="images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="css/swiper.min.css">
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/custom_mobile.css">

    <script type="text/javascript">
        <!--
        if(screen.width >= 640){
            document.location = "index.html";
        }
        //-->
    </script>

</head>
<body>

<!-- Header -->

<header>
    <div class="row">
        <div class="small-6 columns text-left">
            <a href="/">
                <img src="images/logo.png" class="header-logo"/>
            </a>
        </div>
        <div class="small-6 columns text-right" style="padding: 10px 15px;">
            <svg id="menuMobile" class="icon-bars footer-social" style="width: 30px; fill: white; height: 40px;">
                <use xlink:href="images/icons.svg#icon-bars"></use>
            </svg>
            <div id="header-menu">
                <ul>
                    <li>
                        THE PROJECT
                        <ul class="header-submenu">
                            <li>CHARACTERS</li>
                            <li>ZESTRIAS</li>
                            <li>SYNCHRO</li>
                            <li>WORLD</li>
                        </ul>
                    </li>
                    <li>BLOG</li>
                    <li>ABOUT</li>
                </ul>
                <div style="text-align: center;">
                    <img src="images/UnitedKingsom.png" class="header-language"/>
                    <img src="images/Spain.png" class="header-language"/>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- HOME -->

<main>
    <div class="topnav-space"></div>
    <div id="home-portal">
        <div class="row">
            <div class="small-12 column text-center">
                <img src="images/logo.png" class="home-logo">
            </div>
        </div>
        <div class="row">
            <div class="small-12 column text-center">
                <section class="home-portal-center">
                    <iframe width="88%" height="236" src="https://www.youtube.com/embed/dCHdscWRCBA" frameborder="0" allowfullscreen></iframe>
                </section>
                <div class="home-portal-left">
                    <section id="link_characters_m" class="home-portal-section section-1">
                        CHARACTERS
                    </section>
                    <section id="link_zestrias_m" class="home-portal-section section-2">
                        ZESTRIAS
                    </section>
                </div>
                <div class="home-portal-right">
                    <section id="link_sincro_m" class="home-portal-section section-3">
                        SYNCHRO
                    </section>
                    <section id="link_world_m" class="home-portal-section section-4">
                        WORLD
                    </section>
                </div>
            </div>
        </div>
    </div>

    <div id="home-characters_m" style="background-color: #995353;">
        <div class="row margin-0">
            <div class="small-12 columns" style="background: rgba(0,0,0,0.8);">
                <div class="home-section-title">CHARACTER</div>
                <div class="home-section-description">
                    <b>Kyron</b><br>
                    <span>Kyron is 16 when the army destroys his village in Wagens and his family. After discovering his true nature as shepherd, he starts his journey with Kintia and Temy, his Zestria and best friend, to find the keys and protect Proxyma.</span>
                </div>
            </div>
            <div class="small-12 columns text-center">
                <div class="home-section-characterZestria">
                    <!-- Slider main container -->
                    <div class="swiper-container">
                        <!-- Additional required wrapper -->
                        <div id="swiper-characters" class="swiper-wrapper">
                            <!-- Slides -->
                            <div class="swiper-slide">
                                <img src="images/pj/kyron.png" class="characterPersonaje">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/pj/kinttia.png" class="characterPersonaje">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/pj/ylva.png" class="characterPersonaje">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/pj/tane.png" class="characterPersonaje">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/pj/feng.png" class="characterPersonaje">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/pj/zoe.png" class="characterPersonaje">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/pj/alastor.png" class="characterPersonaje">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/pj/edda.png" class="characterPersonaje">
                            </div>
                        </div>
                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="home-zestrias_m" style="background-color: #58829B;">
        <div class="row margin-0">
            <div class="small-12 columns" style="background: rgba(0,0,0,0.8);">
                <div class="home-section-title">ZESTRIAS</div>
                <div class="home-section-description">
                    <b>Temmy</b><br>
                    <span>Temmy is a blue tadpole zestria with wings. It’s Kyron’s partner and best friend. They met when Temmy was born from an egg that Kyron had found inside some ruins in the woods when he was 5 years old.</span>
                </div>
            </div>
            <div class="small-12 columns text-center">
                <div class="home-section-characterZestria">
                    <!-- Slider main container -->
                    <div class="swiper-container">
                        <!-- Additional required wrapper -->
                        <div id="swiper-zestrias" class="swiper-wrapper">
                            <!-- Slides -->
                            <div class="swiper-slide">
                                <img src="images/zestrias/temmy.png" class="characterZestria">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/zestrias/neri.png" class="characterZestria">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/zestrias/aki.png" class="characterZestria">
                            </div>
                        </div>
                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="home-sincro_m" style="background-color: #7C5991;">
        <div class="row margin-0">
            <div class="small-12 columns" style="background: rgba(0,0,0,0.8);">
                <div class="home-section-title">SYNCHRO</div>
                <div class="home-section-description">
                    The synchro is a magic ability that appeared after the orb shattered.
                    Its fragments, the crystals  that are needed to perform it, are inside the zestrias.
                    It allows the human to fuse with the zestria, thus a new form is born with new powers.
                    Just a few people are able to perform it, they are known as the synblers.
                </div>
            </div>
            <div class="small-12 columns text-center">
                <div class="home-section-characterZestria">
                    <!-- Slider main container -->
                    <div class="swiper-container">
                        <!-- Additional required wrapper -->
                        <div id="swiper-synchro" class="swiper-wrapper">
                            <!-- Slides -->
                            <div class="swiper-slide">
                                <img src="images/sincro/kyron_synchro.png" class="characterSincro">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/sincro/kinttia_synchro.png" class="characterSincro">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/sincro/ylva_synchro.png" class="characterSincro">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/sincro/zoe_synchro.png" class="characterSincro">
                            </div>
                            <div class="swiper-slide">
                                <img src="images/sincro/alastor_synchro.png" class="characterSincro">
                            </div>
                        </div>
                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="home-world_m" class="alkoi">
        <div class="row margin-0">
            <div class="small-12 columns text-center"></div>
            <div class="small-12 columns columns" style="background: rgba(0,0,0,0.8);">
                <div class="home-section-title">WORLD</div>
                <div class="home-section-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
            </div>
        </div>
    </div>
</main>

<!-- FOOTER -->

<footer>
    <div class="row">
        <div class="large-12 column text-center" style="line-height: 30px;">
            <div class="footer-wrapper">
                <div class="footer-text-wrapper">
                    All rights reserved
                </div>
                <div class="footer-text-wrapper">
                    &copy; <?php echo date("Y"); ?> Hmhstudios
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- OCULTACION -->

<section>
    <div id="puerta-arriba">
        <div id="puerta-cerrojo">
            <input id="cerrojo_key" type="password" required>
            <img src="images/denied.gif" class="nope" />
        </div>
    </div>
    <div id="puerta-abajo"></div>
</section>

<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>
<script src="js/app.js"></script>
<script src="js/swiper.jquery.min.js"></script>
<!-- Initialize Swiper -->
<script>

</script>
<script src="js/home_mobile.js"></script>
</body>
</html>
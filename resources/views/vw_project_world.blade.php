<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>The Eternal Shepherd - Towers of Proxyma</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{url('images/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" href="{{url('images/favicon/favicon-32x32.png')}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{url('images/favicon/favicon-16x16.png')}}" sizes="16x16">
    <link rel="manifest" href="{{url('images/favicon/manifest.json')}}">
    <link rel="mask-icon" href="{{url('images/favicon/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/foundation/foundation.css')}}">
    <link rel="stylesheet" href="{{url('css/app.css')}}">
    <link rel="stylesheet" href="{{url('css/custom/stylesheets/custom.css')}}">
    <link rel="stylesheet" href="{{url('css/custom/stylesheets/custom_mobile.css')}}">

    <script type="text/javascript">
      <!--
      if (screen.width <= 639) {
        {{ redirect('./home_mobile.html')}}
      }
      //-->
    </script>

  </head>
  <body>

  <!-- Header -->

    <header>
      <div class="row">
        <div class="small-4 medium-4 large-3 columns text-left">
          <a href="/">
            <img src="images/logo.png" class="header-logo"/>
          </a>
        </div>
        <div class="small-8 medium-8 large-9 columns">
          <div id="header-menu">
            <div style="float: right">
              <img src="images/UnitedKingsom.png" class="header-language"/>
              <img src="images/Spain.png" class="header-language"/>
            </div>
            <ul>
              <li>
                THE PROJECT
                <ul class="header-submenu">
                  <a href="{{url('/characters')}}">
                    <li>CHARACTERS</li>
                  </a>
                  <a href="{{url('/zestrias')}}">
                    <li>ZESTRIAS</li>
                  </a>
                  <a href="{{url('/synchro')}}">
                    <li>SYNCHRO</li>
                  </a>
                  <a href="{{url('/world')}}">
                    <li>WORLD</li>
                  </a>
                </ul>
              </li>
              <a href="{{url('/blog')}}">
                <li>BLOG</li>
              </a>
              <a href="{{url('/about')}}">
                <li>ABOUT</li>
              </a>
            </ul>
          </div>
        </div>
      </div>
    </header>

    WORLD
    
  </body>
</html>
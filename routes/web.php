<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('vw_home');
});

Route::get('/characters', function () {
    return view('vw_project_characters');
});

Route::get('/zestrias', function () {
    return view('vw_project_zestrias');
});

Route::get('/synchro', function () {
    return view('vw_project_synchro');
});

Route::get('/world', function () {
    return view('vw_project_world');
});

Route::get('/blog', function () {
    return view('vw_blog');
});

Route::get('/about', function () {
    return view('vw_about');
});
